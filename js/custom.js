(function(){

"use strict";
'use strict';

/*------------------ code for Google Analytics ------------------------------*/
angular.module('googleAnalytics', []);
angular.module('googleAnalytics').run(function ($rootScope, $interval, analyticsOptions) {
	if(analyticsOptions.hasOwnProperty("enabled") && analyticsOptions.enabled) {
		if(analyticsOptions.hasOwnProperty("siteId") && analyticsOptions.siteId != '') {
			if(typeof ga === 'undefined') {
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

				ga('create', analyticsOptions.siteId, {'alwaysSendReferrer': true});
				ga('set', 'anonymizeIp', true);
			}
		}
		$rootScope.$on('$locationChangeSuccess', function (event, toState, fromState) {
			if(analyticsOptions.hasOwnProperty("defaultTitle")) {
				var documentTitle = analyticsOptions.defaultTitle;
				var interval = $interval(function () {
					if(document.title !== '') documentTitle = document.title;
					if (window.location.pathname.indexOf('openurl') !== -1 || window.location.pathname.indexOf('fulldisplay') !== -1)
						if (angular.element(document.querySelector('prm-full-view-service-container .item-title>a')).length === 0) return;
						else documentTitle = angular.element(document.querySelector('prm-full-view-service-container .item-title>a')).text();
					
					if(typeof ga !== 'undefined') {
						if(fromState != toState) ga('set', 'referrer', fromState);
						ga('set', 'location', toState);
						ga('set', 'title', documentTitle);
						ga('send', 'pageview');
					}
					$interval.cancel(interval);
				}, 0);
			}
		});
	}
});
angular.module('googleAnalytics').value('analyticsOptions', {
	enabled: true,
	siteId: 'UA-384382-26',
	defaultTitle: 'PrimoVE Discovery Search'
});
/*------------------ end code for Google Analytics ------------------------------*/

var app = angular.module('viewCustom', ['angularLoad', 'googleAnalytics']);


(function() {
	/*------------------ code for libchat widget ------------------------------------*/
    /* https://developers.exlibrisgroup.com/blog/embedding-springshare-libchat-widget-into-the-primo-nu/ */
    var lc = document.createElement('script');
    lc.type = 'text/javascript';
    lc.async = 'true';
    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'region-ca.libanswers.com/load_chat.php?hash=066533fbd9dc994a7032856801873c71';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(lc, s);
	/*--------------- end code for libchat widget ------------------------------------*/
})();

/******************* Adding Report a Problem link ***************************/
// code from https://libguides.colostate.edu/c.php?g=701450&p=6569946
app.constant('reportProblemOptions', {
	probbutton: "Report a problem",
	problinkbase: "https://uwaterloo.ca/library/report-omni-problem",
	askbutton: "Ask Us",
	asklink: "https://uwaterloo.ca/library/services/ask-us/"
});

app.controller('prmActionContainerAfterController', ['$document', '$scope', '$location', 'reportProblemOptions',
	function ($document, $scope, $location, reportProblemOptions) {
		$scope.probbutton = reportProblemOptions.probbutton;
		$scope.problink = `${reportProblemOptions.problinkbase}?referrer=${encodeURIComponent($location.absUrl())}`;
		$scope.askbutton = reportProblemOptions.askbutton;
		$scope.asklink = reportProblemOptions.asklink;
	}]
);

app.component('prmActionContainerAfter', {
	bindings: { parentCtrl: '<' },
	controller: 'prmActionContainerAfterController',
	template: '\
	<div class="bar filter-bar layout-align-center-center layout-row margin-top-medium" layout="row" layout-align="center center" id="primo-library-help">\
		<a target="_blank" ng-href="{{ problink }}" id="anchor-primo-library-help" ng-click="events.showPermalink()">\
			<button class="button-with-icon zero-margin md-button md-button-raised md-primoExplore-theme md-ink-ripple" type="button" aria-label="Report a problem" >\
				<img alt="Report a Problem" width="24px" src="https://lib.uwaterloo.ca/images/primo_report_problem.png" />\
				<span style="text-transform: none;">{{ probbutton }}</span>\
				<div class="md-ripple-container"></div>\
			</button>\
		</a>\
	</div>\
	'
});
/******************* end - Adding Report a Problem link ***************************/

})();
